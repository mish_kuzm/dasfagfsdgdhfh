// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "tps/FuncLibrary/TTypes.h"
#include "tpsCharacter.generated.h"

UCLASS(Blueprintable)
class AtpsCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AtpsCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeedMultiplier;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	//	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		//	bool SprintRunEnabled = false;
	//	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		//	bool WalkEnabled = false;
		UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
			bool AimEnabled = false;


	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);

		float AxisX = 0.0f;
		float AxisY = 0.0f;

	//Tick Function
		UFUNCTION()
			void MovementTick(float DeltaTime);

		UFUNCTION(BlueprintCallable)
			void CharacterUpdate();
		UFUNCTION(BlueprintCallable)
			void ChangeMovementState(EMovementState NewMovementState);

// Stamina properties........................................................................
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
			float Stamina;

			bool bCanShrinkStamina;
			bool bCanIncreaseStamina;

		UFUNCTION()
			void StartSprinting();

		UFUNCTION()
			void StopSprinting();

			void UpdateStamina(float DeltaTime);
};


﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "tpsCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
//#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "tps/FuncLibrary/TTypes.h"

AtpsCharacter::AtpsCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	// Initialize stamina properties
	Stamina = 100.0f;
	bCanShrinkStamina = false;
	bCanIncreaseStamina = true;
}

void AtpsCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);

	if (MovementState == EMovementState::SprintRun_State)
	{
		FVector ControlForward = GetVelocity().GetSafeNormal();
		const FVector ForwardVector = GetActorForwardVector();
		const float DotProduct = FVector::DotProduct(ControlForward, ForwardVector);
		if (DotProduct < 0.7f)
		{
			ChangeMovementState(EMovementState::Run_State);
		}
	}

 //Update stamina logic.......................................
	UpdateStamina(DeltaSeconds);

	// Display stamina value on screen......................................
	if (GEngine)
	{
		FString StaminaString = FString::Printf(TEXT("Stamina: %f"), Stamina);
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Green, StaminaString);
	}
}

void AtpsCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis(TEXT("MoveForward"), this, &AtpsCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &AtpsCharacter::InputAxisY);

	//PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AtpsCharacter::StartSprinting);
	//PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AtpsCharacter::StopSprinting);
}

void AtpsCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void AtpsCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void AtpsCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult ResultHit;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		
		SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw, 0.0f)));
	}
}

void AtpsCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	FCharacterSpeed f;
	switch (MovementState)
	{
		case EMovementState::Aim_State:
			ResSpeed = f.AimSpeedNormal;
			break;
		case EMovementState::AimWalk_State:
			ResSpeed = f.AimSpeedWalk;
			break;
		case EMovementState::Walk_State:
			ResSpeed = f.WalkSpeedNormal;
			break;
		case EMovementState::Run_State:
				ResSpeed = f.RunSpeedNormal;
				break;
		case EMovementState::SprintRun_State:
				ResSpeed = f.SprintRunSpeedRun;
				break;
			default:
				break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AtpsCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	if (!AimEnabled) //если игрок не целится, дополнительные проверки не нужны
	{
		MovementState = NewMovementState;
		CharacterUpdate();
		return;
	}

	if (NewMovementState == EMovementState::Run_State || NewMovementState == EMovementState::Walk_State || NewMovementState == EMovementState::SprintRun_State)
	{
		MovementState = EMovementState::AimWalk_State; //если игрок идет/бежит, то ставим скорость ходьбы + прицела
	}
	else
	{
		MovementState = EMovementState::Aim_State; //иначе он просто целится
	}

	CharacterUpdate();
}

void AtpsCharacter::UpdateStamina(float DeltaTime)
{
	if (MovementState == EMovementState::SprintRun_State && Stamina > 0)//(bCanShrinkStamina && Stamina > 0)
	{
		Stamina -= 20.0f * DeltaTime; // Регулируйте скорость уменьшения выносливости по мере необходимости
		if (Stamina <= 0)
		{
			ChangeMovementState(EMovementState::Run_State);
			//Stamina = 0;
			//StopSprinting();
		}
	}
	else //(bCanIncreaseStamina && Stamina < 100)
	{
		Stamina += 20.0f * DeltaTime; // Регулируйте скорость восстановления выносливости по мере необходимости
		if (Stamina >= 100.0f)
		{
			Stamina = 100.0f;
		}
	}
}

void AtpsCharacter::StartSprinting()
{
	bCanShrinkStamina = true;
	bCanIncreaseStamina = false;
}

void AtpsCharacter::StopSprinting()
{
	bCanShrinkStamina = false;
	bCanIncreaseStamina = true;
}
